document.addEventListener('DOMContentLoaded', function() {
    // Получаем все элементы с классом 'clickable'
    var elements = document.querySelectorAll('.clickable');

    // Добавляем обработчик события клика для каждого элемента
    elements.forEach(function(element) {
        element.addEventListener('click', function() {
            // Удаляем класс 'clicked' у всех элементов
            elements.forEach(function(el) {
                el.classList.remove('clicked');
            });

            // Добавляем класс 'clicked' к нажатому элементу
            this.classList.add('clicked');
        });
    });
});