
/*
const salary = {'Коля': '1000', 'Вася': '500', 'Петя': '200'};     //     1
document.write('Зарплата Пети: ' + salary['Петя'] + '<br>');
document.write('Зарплата Коли: ' + salary['Коля'] + '<br>');

function isEmpty(obj) {                                             // 3  
    return Object.keys(obj).length === 0;
}

const obj = {}; 
const result = isEmpty(obj);
console.log(result);
*/
                                                         // 4
const data = {
    "date": "01.07.2024",
    "bank": "PB",
    "baseCurrency": 980,
    "baseCurrencyLit": "UAH",
    "exchangeRate": [
        {"baseCurrency":"UAH","currency":"AUD","saleRateNB":26.9182000,"purchaseRateNB":26.9182000},
        {"baseCurrency":"UAH","currency":"AZN","saleRateNB":23.8120000,"purchaseRateNB":23.8120000},
        {"baseCurrency":"UAH","currency":"BYN","saleRateNB":14.7042000,"purchaseRateNB":14.7042000},
        {"baseCurrency":"UAH","currency":"CAD","saleRateNB":29.5200000,"purchaseRateNB":29.5200000},
        {"baseCurrency":"UAH","currency":"CHF","saleRateNB":44.9466000,"purchaseRateNB":44.9466000,"saleRate":45.4000000,"purchaseRate":44.7300000},
        {"baseCurrency":"UAH","currency":"CNY","saleRateNB":5.5702000,"purchaseRateNB":5.5702000},
        {"baseCurrency":"UAH","currency":"CZK","saleRateNB":1.7297000,"purchaseRateNB":1.7297000,"saleRate":1.7500000,"purchaseRate":1.7200000},
        {"baseCurrency":"UAH","currency":"DKK","saleRateNB":5.8017000,"purchaseRateNB":5.8017000},
        {"baseCurrency":"UAH","currency":"EUR","saleRateNB":43.2658000,"purchaseRateNB":43.2658000,"saleRate":44.0000000,"purchaseRate":43.0000000},
        {"baseCurrency":"UAH","currency":"GBP","saleRateNB":51.1382000,"purchaseRateNB":51.1382000,"saleRate":51.7300000,"purchaseRate":50.9700000},
        {"baseCurrency":"UAH","currency":"GEL","saleRateNB":14.4490000,"purchaseRateNB":14.4490000},
        {"baseCurrency":"UAH","currency":"HUF","saleRateNB":0.1094780,"purchaseRateNB":0.1094780},
        {"baseCurrency":"UAH","currency":"ILS","saleRateNB":10.7740000,"purchaseRateNB":10.7740000},
        {"baseCurrency":"UAH","currency":"JPY","saleRateNB":0.2518600,"purchaseRateNB":0.2518600},
        {"baseCurrency":"UAH","currency":"KZT","saleRateNB":0.0855630,"purchaseRateNB":0.0855630},
        {"baseCurrency":"UAH","currency":"MDL","saleRateNB":2.2550000,"purchaseRateNB":2.2550000},
        {"baseCurrency":"UAH","currency":"NOK","saleRateNB":3.7962000,"purchaseRateNB":3.7962000},
        {"baseCurrency":"UAH","currency":"PLN","saleRateNB":10.0410000,"purchaseRateNB":10.0410000,"saleRate":10.2000000,"purchaseRate":10.0300000},
        {"baseCurrency":"UAH","currency":"SEK","saleRateNB":3.8061000,"purchaseRateNB":3.8061000},
        {"baseCurrency":"UAH","currency":"SGD","saleRateNB":29.8291000,"purchaseRateNB":29.8291000},
        {"baseCurrency":"UAH","currency":"TMT","saleRateNB":11.5583000,"purchaseRateNB":11.5583000},
        {"baseCurrency":"UAH","currency":"TRY","saleRateNB":1.2308000,"purchaseRateNB":1.2308000},
        {"baseCurrency":"UAH","currency":"UAH","saleRateNB":1.0000000,"purchaseRateNB":1.0000000},
        {"baseCurrency":"UAH","currency":"USD","saleRateNB":40.4542000,"purchaseRateNB":40.4542000,"saleRate":40.8500000,"purchaseRate":40.2500000},
        {"baseCurrency":"UAH","currency":"UZS","saleRateNB":0.0032215,"purchaseRateNB":0.0032215},
        {"baseCurrency":"UAH","currency":"XAU","saleRateNB":94428.6000000,"purchaseRateNB":94428.6000000}
    ]
};

function renderTable(exchangeRates) {
    const tableBody = document.getElementById('currencyTableBody');
    tableBody.innerHTML = '';

    exchangeRates.forEach(rate => {
        const buyPrice = rate.purchaseRate || rate.purchaseRateNB;
        const sellPrice = rate.saleRate || rate.saleRateNB;

        const row = document.createElement('tr');
        
        const nameCell = document.createElement('td');
        nameCell.textContent = rate.currency;
        row.appendChild(nameCell);
        
        const buyCell = document.createElement('td');
        buyCell.textContent = buyPrice.toFixed(2); 
        row.appendChild(buyCell);
        
        const sellCell = document.createElement('td');
        sellCell.textContent = sellPrice.toFixed(2);
        row.appendChild(sellCell);
        
        tableBody.appendChild(row);
    });
}

function init() {
    const exchangeRates = data.exchangeRate;
    renderTable(exchangeRates);
}

init();