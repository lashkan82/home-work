
const cryptoWallet = {};

cryptoWallet.name = "Danila";

cryptoWallet.btc = {
    name: "Bitcoin",
    logo: "https://cryptologos.cc/logos/bitcoin-btc-logo.png?v=025",
    amount: 2,
    rate: 2479979.14 // курс в гривнах за 1 штуку
};

cryptoWallet.eth = {
    name: "Ethereum",
    logo: "https://cryptologos.cc/logos/ethereum-eth-logo.png?v=025",
    amount: 5,
    rate: 108013.68 // курс в гривнах за 1 штуку
};

cryptoWallet.stl = {
    name: "Stellar",
    logo: "https://cryptologos.cc/logos/stellar-xlm-logo.png?v=025",
    amount: 102,
    rate: 4.14 // курс в гривнах за 1 штуку
};

const ownerName = cryptoWallet.name;

const h1 = document.createElement('h1');

h1.textContent = ownerName;

const mainElement = document.querySelector('main');
const spanElement = document.querySelector('span')

mainElement.appendChild(h1);

const select = document.createElement('select');

for (const key in cryptoWallet) {
    if (cryptoWallet[key].name) {
        const option = document.createElement('option');
        option.value = key;
        option.textContent = cryptoWallet[key].name;
        select.appendChild(option);
    }
}

spanElement.appendChild(select);

const messageContainer = document.createElement('p');
spanElement.appendChild(messageContainer);

select.addEventListener('change', function () {
    const selectedCrypto = cryptoWallet[this.value];

    const totalValue = selectedCrypto.amount * selectedCrypto.rate;

    const message = `
    Добрый день, ${ownerName}! На вашем балансе ${selectedCrypto.name} 
    <img src="${selectedCrypto.logo}" alt="${selectedCrypto.name} logo" style="width: 20px; vertical-align: middle; margin-left: 8px;">
    осталось ${selectedCrypto.amount} монет. Если вы сегодня продадите их, то получите ${totalValue.toFixed(2)} гривен.
`;


   // messageContainer.textContent = message;
   messageContainer.innerHTML = message;
});
