document.addEventListener('DOMContentLoaded', function () {
    var image = document.getElementById('hover-image');
    var description = document.getElementById('image-description');

    image.addEventListener('mouseover', function () {
        description.style.display = 'block';
    });

    image.addEventListener('mouseout', function () {
        description.style.display = 'none';
    });
});