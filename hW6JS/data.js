class worker {                                       // lesson 1
    constructor(name,surname,rate,days) {
    }
}               

worker.name = "Danila";
worker.surname = "Lascan";
worker.rateHours = 7.5;
worker.hours = 8;
worker.days = 21;

worker.getSalary = function () {
    return  this.rateHours * this.hours * this.days;
}

salaryPrint = worker.getSalary();

document.write(salaryPrint)


const MyString = function() {                           // lesson 2

    this.reverse = function(string) {
        return string.split('').reverse().join('');

    };

    this.ucFirst = function(string) {
        if (!string) return string;
        return string.charAt(0).toUpperCase() + string.slice(1);

    };

    this.ucWords = function(string) {
        return string.split('').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join('');
    }
};

const myString = new MyString();
console.log(myString.reverse('Hello'));
console.log(myString.ucFirst('hello world'));
console.log(myString.ucWords('hello my world'));




