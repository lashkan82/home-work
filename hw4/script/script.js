function scrollToAnchor(character) {
    var element = document.getElementById(character);
    if (element) {
        element.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
}

