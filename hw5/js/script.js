function scrollToAnchor(header) {
    var element = document.getElementById(header);
    if (element) {
        element.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
}